//
//  MovieSearchViewController.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/4/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import UIKit

class MovieSearchViewController: UIViewController {

    @IBOutlet weak var totalMoviesCountLabel: UILabel!
    @IBOutlet weak var moviesTableView: UITableView!

    let searchController = UISearchController(searchResultsController: nil)
    
    var viewModel = MovieSearchViewModel()
    var currentPage = 1
    var currentSearchText = ""
    
    private var previousRun = Date()
    private let minInterval = 0.05
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.delegate = self
        configureTableView()
        configureSearchController()
        moviesTableView.configureBackgroundView(with: Constants.noSearchResults)
        configueTapGesture()
    }
    
}


// MARK: TableView DataSource and Delegate Methods
extension MovieSearchViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getCurrentPageMoviesCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let movieCell = tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifiers.movieTableViewCell, for: indexPath) as? MovieTableViewCell {
            
            if let movie = viewModel.getMovie(at: indexPath.row) {
                movieCell.configureCell(with: movie)
            }
            
            return movieCell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let movieID = viewModel.getMovie(at: indexPath.row)?.movieID else {
            return
        }
        let movieDetailsVC = MovieDetailsRouter.instantiateMovieDetailsViewController(for: movieID)
        
        self.navigationController?.pushViewController(movieDetailsVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == viewModel.getCurrentPageMoviesCount() - 1  && currentPage <  viewModel.getMaxPage() {
            currentPage += 1
            findMovies(keyword: currentSearchText)
            searchController.searchBar.resignFirstResponder()
        }
        
        // this will turn on `masksToBounds` just before showing the cell
        cell.contentView.layer.masksToBounds = true
        
        // if you do not set `shadowPath` you'll notice laggy scrolling
        // add this in `willDisplay` method
        let radius = cell.contentView.layer.cornerRadius
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: radius).cgPath

    }
    
}

// MARK: ViewModelUIDelegate Methods
extension MovieSearchViewController: ViewModelUpdateUIDelegate{
    
    func willBeginDataRequests() {
        showLoader(true)
    }
    
    func didCompleteWithError(alertMessage: AlertMessage) {
        showLoader(false)
        showAlert(with: alertMessage)
    }
    
    func didCompleteDataRequests() {
        
       showLoader(false)
        
        if let totalMoviesCount = viewModel.getTotalSearchResultCount() {
            totalMoviesCountLabel.text = String(totalMoviesCount)
        }
        reloadData()
    }
   
}

// MARK: UISearchBarDelegate Methods
extension MovieSearchViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.resetSearch()
        guard let textToSearch = searchBar.text, !textToSearch.isEmpty else {
            return
        }
        currentSearchText = searchText
        if Date().timeIntervalSince(previousRun) > minInterval {
            previousRun = Date()
            findMovies(keyword: searchText)
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        currentSearchText = ""
        totalMoviesCountLabel.text = "0"
        viewModel.resetSearch()
        reloadData()
    }
    
}

// MARK: MovieSearchViewController Private Helpers
private extension MovieSearchViewController {
    
    func configureTableView() {
        moviesTableView.register(UINib(nibName: TableViewCellIdentifiers.movieTableViewCell, bundle: nil), forCellReuseIdentifier: TableViewCellIdentifiers.movieTableViewCell)
        moviesTableView.estimatedRowHeight = 80.0
        moviesTableView.rowHeight = UITableView.automaticDimension
    }
    
    func configureSearchController() {
        
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search for a Movie"

        navigationItem.searchController = searchController
    }
    
    func configueTapGesture() {
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.singleTap(sender:)))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(singleTapGestureRecognizer)
    }
    
    @objc func singleTap(sender: UITapGestureRecognizer) {
        searchController.searchBar.resignFirstResponder()
    }
    
    func reloadData() {
        moviesTableView.reloadData()
    }
    
    func findMovies(keyword: String) {
        viewModel.search(for: keyword, at: currentPage)
    }
    
}

