//
//  MovieSearchViewModel.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/4/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation

class MovieSearchViewModel: ViewModel {
    
    private var movieResults: MovieResults?
    
    var handler: Handler {
        return MoviesHandler()
    }
    
    var delegate: ViewModelUpdateUIDelegate?
    
    func search(for keyword: String, at page: Int) {
        
        delegate?.willBeginDataRequests()
       
        guard let handler = handler as? MoviesHandler else {
            return
        }
        
        handler.searchMovies(keyword: keyword, page: page) { (result: Swift.Result<MovieResults, AlertMessage>) in
            
            switch result {
            case .success(let currentPageResults):
                
                //For paging
                if let movies = self.movieResults {
                    let newData = movies.results + currentPageResults.results
                    self.movieResults?.results = newData
                } else {
                    self.movieResults = currentPageResults
                }
                
                self.delegate?.didCompleteDataRequests()
                
            case .failure(let errorMessage):
                self.delegate?.didCompleteWithError(alertMessage: errorMessage)
            }
        }
    }
    
    func resetSearch() {
        movieResults = nil
    }
    
    func getCurrentPageMoviesCount() -> Int {
        return movieResults?.results.count ?? 0
    }
    
    func getMovie(at index: Int) -> Movie? {
        return movieResults?.results[index]
    }
    
    func getTotalSearchResultCount() -> Int? {
        return movieResults?.totalResults
    }
    
    func getMaxPage() -> Int {
        return movieResults?.totalPages ?? 0
    }
}
