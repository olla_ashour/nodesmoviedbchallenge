//
//  UIViewController+Extensions.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/4/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import UIKit
import MBProgressHUD

extension UIViewController {
    
    func showAlert(with message: AlertMessage , style: UIAlertController.Style = .alert) {
        let alertController = UIAlertController(title: message.title, message: message.body, preferredStyle: style)
        let action = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showLoader(_ show: Bool) {
        if show {
            MBProgressHUD.showAdded(to: self.view, animated: true)
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
}
