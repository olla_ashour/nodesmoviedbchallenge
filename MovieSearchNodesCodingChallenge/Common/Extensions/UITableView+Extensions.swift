//
//  UITableView+Extensions.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/13/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import UIKit

extension UITableView {
    
    func configureBackgroundView(with text: String) {
        self.tableFooterView = UIView()
        let backgroundViewLabel = UILabel(frame: .zero)
        backgroundViewLabel.textColor = .darkGray
        backgroundViewLabel.numberOfLines = 0
        backgroundViewLabel.text = text
        backgroundViewLabel.textAlignment = NSTextAlignment.center
        backgroundViewLabel.font.withSize(20)
        self.backgroundView = backgroundViewLabel
    }
    
}
