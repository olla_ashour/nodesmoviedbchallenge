//
//  String+Extensions.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/5/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation

extension String {
    
    func getYear() -> Int {
 
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let calendar = Calendar.current
       
        guard let formattedDate = dateFormatter.date(from:self) else {
            return calendar.component(.year, from: Date())
        }
        
        return calendar.component(.year, from: formattedDate)
    }
    
}
