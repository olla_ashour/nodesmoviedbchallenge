//
//  Constants.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/3/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation

struct Constants {
    static let productionBaseURL = "https://api.themoviedb.org/3"
    static let stagingBaseURL = "https://api.themoviedb.org/3"
    static let qaBaseURL = "https://api.themoviedb.org/3"
    static let imageBaseURL = "https://image.tmdb.org/t/p/w500"
    static let apiKey = "4cb1eeab94f45affe2536f2c684a5c9e"
    static let noSearchResults = " Search to Display Results ";
    static let noFavorites = " Add to Favorites to Display Data"
}

struct TableViewCellIdentifiers {
    static let movieTableViewCell = "MovieTableViewCell"
}

struct Storyboards {
    static let mainStoryboard = "Main"
}

struct ViewControllers {
    static let movieDetailsVC = "MovieDetailsViewController"
}
