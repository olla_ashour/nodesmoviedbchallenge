//
//  AlertMessage.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/3/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//  Copyright © 2019 Pavle Pesic. All rights reserved.

import Foundation

struct AlertMessage: Error {
    
    var title = ""
    var body = ""
    
    init(title: String, body: String) {
        self.title = title
        self.body = body
    }
    
}
