//
//  NetworkError.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/4/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation

struct NetworkError: Codable {
    
    let errors: [String]
    
    private enum CodingKeys: String, CodingKey {
        case errors
    }
}
