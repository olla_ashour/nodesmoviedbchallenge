//
//  Movie.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/3/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation

class Movie: Decodable {
    
    var movieID: Int
    var title: String
    var rating: Double
    var releaseDate: String
    var genres: [Genre]?
    var overview: String
    var runtime: Int?
    var status: String?
    var posterURL: String?  
    var voteCount: Int?
    
    private enum CodingKeys: String, CodingKey {
        
        case title, overview, runtime, status, genres
        case movieID = "id"
        case rating = "vote_average"
        case releaseDate = "release_date"
        case posterURL = "poster_path"
        case voteCount = "vote_count"
    }
    
    required init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)

        movieID = try container.decode(Int.self, forKey: .movieID)
        title = try container.decode(String.self, forKey: .title)
        rating = try container.decode(Double.self, forKey: .rating)
        releaseDate = try container.decode(String.self, forKey: .releaseDate)
        overview = try container.decode(String.self, forKey: .overview)
        runtime = try container.decodeIfPresent(Int.self, forKey: .runtime)
        voteCount = try container.decodeIfPresent(Int.self, forKey: .voteCount)
        status = try container.decodeIfPresent(String.self, forKey: .status)
        posterURL = try container.decodeIfPresent(String.self, forKey: .posterURL)
        genres = try container.decodeIfPresent([Genre].self, forKey: .genres)
    }
}



