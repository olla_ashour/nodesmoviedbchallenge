//
//  MovieTableViewCell.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/4/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import UIKit
import SDWebImage
import Cosmos

class MovieTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        resetData()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setStyle()
    }
    
     //TODO: Improve -- add cell model
    func configureCell(with movie: Movie) {
        
        titleLabel.text = movie.title
        overviewLabel.text = movie.overview
        releaseDateLabel.text = movie.releaseDate
        ratingView.rating = movie.rating / 2
        
        //TODO: Improve handling of API key concat 
        if let posterURL = movie.posterURL {
            let imageURL = Constants.imageBaseURL + posterURL  + "?api_key" + Constants.apiKey
            posterImageView.sd_setImage(with: URL(string: imageURL), placeholderImage: nil, options: .refreshCached, completed: nil)
        }
        
    }
    
    @IBAction func addToFavorites(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
}

// MARK : Private Helpers
private extension MovieTableViewCell {
    
    func resetData() {
        titleLabel.text = nil
        overviewLabel.text = nil
        releaseDateLabel.text = nil
        ratingView.rating = 0
        posterImageView.image = nil
    }
    
    func setStyle() {
        
        // add shadow on cell
        backgroundColor = .clear
        layer.masksToBounds = false
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 5
        layer.shadowOffset = CGSize(width: 1, height: 1)
        layer.shadowColor = UIColor.black.cgColor
        
        // add corner radius on `contentView`
        contentView.backgroundColor = .white
        contentView.layer.cornerRadius = 8
        
    }
    
}

