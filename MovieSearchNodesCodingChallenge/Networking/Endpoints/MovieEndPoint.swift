//
//  MovieEndPoint.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/3/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation
import Alamofire

enum MovieEndpoint {
    case search(keyword: String, page: Int)
    case details(id: Int)
}

extension MovieEndpoint: EndPoint {
    
    var httpMethod: HTTPMethod {
       return .get
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
    
    var paramters: Parameters? {
        switch self {
        case .search(let keyword, let page):
            return ["api_key": Constants.apiKey, "query": keyword, "page": page]
        default:
            return ["api_key": Constants.apiKey]
        }
    }
    
    var path: String {
        switch self {
        case .search:
            return "/search/movie"
        case .details(let id):
            return "/movie/\(id)"
        }
    }
    
}
