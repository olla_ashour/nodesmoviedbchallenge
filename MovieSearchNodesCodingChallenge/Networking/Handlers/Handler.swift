//
//  Handler.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/4/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation

protocol Handler {
    
    var networkManager: NetworkManager { get }
    var parseError: ApplicationError { get }
    var defaultParseAlertMessage: AlertMessage { get }
}

extension Handler {
    
    var networkManager: NetworkManager {
        return NetworkManager()
    }
    
    var parseError: ApplicationError {
        return .apiError(type: .parseError)
    }
    
    var defaultParseAlertMessage: AlertMessage {
        return AlertMessage(title: parseError.title, body: parseError.description)
    }
}
