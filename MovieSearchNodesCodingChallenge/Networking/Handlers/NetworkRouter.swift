//
//  NetworkRouter.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/3/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation

class MoviesHandler {
    
    func searchMovies(keyword: String, page: Int, completion: @escaping (Result<MovieResults, AlertMessage>) -> Void) {

        NetworkManager().sendRequest(for: MovieEndpoint.search(keyword: keyword, page: page)) { (result: Swift.Result<Data?, Error>) in
            
            switch result {
            case .success(let data):
  
                guard let jsonData = data, let searchResults = MovieResults(data: jsonData) else {
                    debugPrint("💥 DECODING ERROR: Movie Search")
                    completion(.failure(AlertMessage(title: Constants.errorAlertTitle, body: Constants.parsingError)))
                    return
                }
                
                completion(.success(searchResults))
    
            case .failure(let error):
                completion(.failure(AlertMessage(title: Constants.errorAlertTitle, body: error.localizedDescription)))
            }
        }
       
    }
    
    func getMovieDetails(for movieID: Int, completion: @escaping (Result<Movie, AlertMessage>) -> Void) {
        
        NetworkManager().sendRequest(for: MovieEndpoint.details(id: movieID)) { (result: Swift.Result<Data?, Error>) in
            
            switch result {
            case .success(let data):
                guard let jsonData = data, let movieDetails = Movie(data: jsonData) else {
                    
                    debugPrint("💥 DECODING ERROR: Movie Details")
                    completion(.failure(AlertMessage(title: Constants.errorAlertTitle, body: Constants.parsingError)))
                    return
                }
              
                completion(.success(movieDetails))
                
            case .failure(let error):
                completion(.failure(AlertMessage(title: Constants.errorAlertTitle, body: error.localizedDescription)))
            }
        }
        
    }
    
  
}
