//
//  NetworkRouter.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/3/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation

class MoviesHandler: Handler {
    
    func searchMovies(keyword: String, page: Int, completion: @escaping (Result<MovieResults, AlertMessage>) -> Void) {

        networkManager.sendRequest(for: MovieEndpoint.search(keyword: keyword, page: page)) { (result: Swift.Result<Data?, ApplicationError>) in
            
            switch result {
            case .success(let data):
                
                
                guard let responseData = data else {
                    debugPrint("💥 DECODING ERROR: Movie Details")
                    completion(.failure(self.defaultParseAlertMessage))
                    return
                }
                
                do {
            
                    let decoder = JSONDecoder()
                    let movieResults = try decoder.decode(MovieResults.self, from: responseData)
                    completion(.success(movieResults))
                    
                    
                }catch {
                    print(error)
                    completion(.failure(self.defaultParseAlertMessage))
                }
                
            case .failure(let error):
                completion(.failure(AlertMessage(title: error.title, body: error.description)))
            }
        }
       
    }
    
    func getMovieDetails(for movieID: Int, completion: @escaping (Result<Movie, AlertMessage>) -> Void) {
        
        networkManager.sendRequest(for: MovieEndpoint.details(id: movieID)) { (result: Swift.Result<Data?, ApplicationError>) in
            
            switch result {
            case .success(let data):
                guard let responseData = data else {
                    debugPrint("💥 DECODING ERROR: Movie Details")
                    completion(.failure(self.defaultParseAlertMessage))
                    return
                }
                
                do {
                    
                    let decoder = JSONDecoder()
                    let movieDetails = try decoder.decode(Movie.self, from: responseData)
                    completion(.success(movieDetails))
                    
                   
                }catch {
                    print(error)
                    completion(.failure(self.defaultParseAlertMessage))
                }
                
            case .failure(let error):
                completion(.failure(AlertMessage(title: error.title, body: error.description)))
            }
        }
        
    }
    
  
}
