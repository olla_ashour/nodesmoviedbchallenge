//
//  EndPoint.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/3/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation
import Alamofire

protocol EndPoint {
    var baseURL: String { get }
    var path: String { get }
    var fullURL: String { get }
    var httpMethod: HTTPMethod { get }
    var encoding: ParameterEncoding { get }
    var headers: HTTPHeaders? { get }
    var paramters: Parameters? { get }
}

enum NetworkEnvironment {
    case qa
    case production
    case staging
}

extension EndPoint {
    
    var baseURL: String {
        switch NetworkManager.environment {
        case .production: return Constants.productionBaseURL
        case .qa: return  Constants.qaBaseURL
        case .staging: return Constants.stagingBaseURL
        }
    }
    
    var encoding: ParameterEncoding {
        return httpMethod == .get ? URLEncoding.default : JSONEncoding.default
    }
    
    var fullURL: String {
        return baseURL + path
    }
    
}

