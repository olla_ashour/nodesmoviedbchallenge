//
//  NetworkManager.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/3/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation
import Alamofire

class NetworkManager {
    
    static let environment: NetworkEnvironment = .production
   
    func sendRequest(for endpoint: EndPoint, completion: @escaping (Swift.Result<Data?, ApplicationError>) -> Void) {
        
        Alamofire.request(endpoint.fullURL, method: endpoint.httpMethod, parameters: endpoint.paramters , encoding:endpoint.encoding, headers: endpoint.headers).responseJSON{ (response) in
            
            switch response.result {
            default:
                let result = self.handleNetworkResponse(response.response, data: response.data)
                completion(result)
            }
        }
    }
    
}

private extension NetworkManager {
    
    //TODO: Handle No internet connection 
    func handleNetworkResponse(_ response: HTTPURLResponse?, data: Data?) -> Swift.Result<Data?, ApplicationError> {
        guard let response = response else {
           
            return .failure(.apiError(type: .responseError))
        }
        switch response.statusCode {
        case 200...299: return .success(data)
        case 401...500: return .failure(parseApiError(data: data))
        case 501...599: return .failure(.apiError(type: .badRequest))
        default: return .failure(.apiError(type: .failed))
        }
    }
    
    private func parseApiError(data: Data?) -> ApplicationError {
        let decoder = JSONDecoder()
        if let jsonData = data, let error = try? decoder.decode(NetworkError.self, from: jsonData) {
            
            return (.apiError(type: .serverError(desc: error.errors[0])))
        }
        return (.apiError(type: .responseError))
    }
    
}
