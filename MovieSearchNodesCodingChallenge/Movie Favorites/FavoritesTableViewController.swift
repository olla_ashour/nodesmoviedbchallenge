//
//  FavoritesViewControllerTableViewController.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/13/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import UIKit

class FavoritesTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.configureBackgroundView(with: Constants.noFavorites)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let movieCell = tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifiers.movieTableViewCell, for: indexPath) as? MovieTableViewCell {
           
            return movieCell
        }
        return UITableViewCell()
    }


}

extension FavoritesTableViewController {
    
  
}
