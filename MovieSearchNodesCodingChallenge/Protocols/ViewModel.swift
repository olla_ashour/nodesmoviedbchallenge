//
//  GenericViewModel.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/4/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation

protocol ViewModel {
    var handler: Handler { get }
    var delegate: ViewModelUpdateUIDelegate? { get }
}

