//
//  ViewModelUIDelegate.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/4/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation

protocol ViewModelUpdateUIDelegate {
    func willBeginDataRequests()
    func didCompleteDataRequests()
    func didCompleteWithError(alertMessage: AlertMessage)
}
