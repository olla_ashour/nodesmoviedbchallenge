//
//  MovieDetailsViewController.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/3/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage

class MovieDetailsViewController: UIViewController {

    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var runtimeLabel: UILabel!
    @IBOutlet weak var totalUsersLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    
    var viewModel: MovieDetailsViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel?.delegate = self
    }

    //TODO: Improve
    private func configureDetails() {
        
        guard let movieDetailsViewModel = viewModel, let movie = movieDetailsViewModel.getMovieDetail() else {
            return
        }
        
        titleLabel.text = movie.title + " (\(movie.releaseDate.getYear())" + ")"
        overviewLabel.text = movie.overview
        statusLabel.text = movie.status
        runtimeLabel.text = String(movie.runtime ?? 0) + " minutes"
        ratingView.rating = movie.rating / 2
        totalUsersLabel.text = String(movie.voteCount ?? 0) + " users"
        
        if let genres = movie.genres {
            genresLabel.text = genres.compactMap({ $0.name }).joined(separator: ", ")
        }
       
        //TODO: Improve handling of API key concat
        if let posterURL = movie.posterURL {
            let imageURL = Constants.imageBaseURL + posterURL  + "?api_key" + Constants.apiKey
            posterImageView.sd_setImage(with: URL(string: imageURL), placeholderImage: nil, options: .refreshCached, completed: nil)
        }
    }
    
}

extension MovieDetailsViewController: ViewModelUpdateUIDelegate {
    
    func willBeginDataRequests() {
        showLoader(true)
    }
    
    func didCompleteWithError(alertMessage: AlertMessage) {
        showLoader(false)
        showAlert(with: alertMessage)
    }
    
    func didCompleteDataRequests() {
        showLoader(false)
        configureDetails()
    }
    
}

