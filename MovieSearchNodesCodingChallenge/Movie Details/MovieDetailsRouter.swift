//
//  MovieDetailsRouter.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/4/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import UIKit

class MovieDetailsRouter {
    
    //TODO: Use coordinator design pattern
    static func instantiateMovieDetailsViewController(for id: Int) -> MovieDetailsViewController {
        
        let storyboard = UIStoryboard(name: Storyboards.mainStoryboard, bundle: nil)
        
        guard let movieViewController: MovieDetailsViewController = storyboard.instantiateViewController(withIdentifier: ViewControllers.movieDetailsVC) as? MovieDetailsViewController else {
            return MovieDetailsViewController()
        }
        
        movieViewController.viewModel = MovieDetailsViewModel(movieID: id)
        return movieViewController
    }
    
}
