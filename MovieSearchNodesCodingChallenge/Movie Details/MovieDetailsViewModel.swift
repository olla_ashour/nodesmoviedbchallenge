//
//  MovieDetailsViewModel.swift
//  MovieSearchNodesCodingChallenge
//
//  Created by Olla Ashour on 6/4/19.
//  Copyright © 2019 Olla Ashour. All rights reserved.
//

import Foundation

class MovieDetailsViewModel: ViewModel {
    
    private var movie: Movie?
    
    var handler: Handler {
        return MoviesHandler()
    }
    
    var delegate: ViewModelUpdateUIDelegate?
    
    private var movieID: Int?
    
    init(movieID: Int) {
        self.movieID = movieID
        retrieveDetails()
    }
    
    private func retrieveDetails() {
        
        delegate?.willBeginDataRequests()
        
        guard let handler = handler as? MoviesHandler, let id = movieID else {
            return
        }
        
        handler.getMovieDetails(for: id) { (result: Swift.Result<Movie, AlertMessage>) in
            
            switch result {
            case .success(let movie):
                
                self.movie = movie
                self.delegate?.didCompleteDataRequests()
                
            case .failure(let errorMessage):
                self.delegate?.didCompleteWithError(alertMessage: errorMessage)
            }
        }
    }
    
    func getMovieDetail() -> Movie? {
        return movie
    }
    
}
